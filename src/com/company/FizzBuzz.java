package com.company;

public class FizzBuzz {

    public FizzBuzz() {
    }

    public String Say(int i) {
        boolean ruleFizz3 = ( i % 10  == 3 ) || ( i % 3 == 0 );
        boolean ruleBuzz5 = ( i % 5 == 0 );

        if ( ruleFizz3 && ruleBuzz5 )
            return "Fizz Buzz";

        if ( ruleFizz3 )
            return "Fizz";

        if ( ruleBuzz5 )
            return "Buzz";

        String result = String.valueOf(i);
        return result;
    }

}
