package com.company;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FizzBuzzTest {

    protected static FizzBuzz fizzBuzz;

    private void assertSayExpected(String expected, int what) {
        String actual = fizzBuzz.Say(what);

        assertEquals(expected, actual);
    }


    @BeforeAll
    public static void setup() {
        fizzBuzz = new FizzBuzz();
    }

    @Test
    public void testSayOne() {
        String expected = "1";

        int what = 1;
        assertSayExpected(expected, what);
    }

    @Test
    public void testSayTwo() {
        String expected = "2";

        assertSayExpected(expected, 2);
    }

    @Test
    public void testSayThree() {
        String expected = "Fizz";

        assertSayExpected(expected, 3);
    }

    @Test
    public void testSayFive() {
        String expected = "Buzz";

        assertSayExpected(expected, 5);
    }

    @Test
    public void testSaySix() {
        String expected = "Fizz";

        assertSayExpected(expected, 6);
    }

    @Test
    public void testSayThirteen() {
        String expected = "Fizz";

        assertSayExpected(expected, 13);
    }

    @Test
    public void testSayFifteen() {
        String expected = "Fizz Buzz";

        assertSayExpected(expected, 15);
    }
}